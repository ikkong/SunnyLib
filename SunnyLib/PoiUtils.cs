﻿using System;
using System.Collections.Generic;

namespace SunnyLib
{
    public class PoiUtils
    {
        /// <summary>
        /// 默认地球半径
        /// </summary>
        private const double EarthRadius = 6371;

        /// <summary>
        /// 计算经纬度点对应正方形4个点的坐标
        /// </summary>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Dictionary<String, double[]> ReturnLlSquarePoint(double longitude,
            double latitude, double distance)
        {
            var squareMap = new Dictionary<String, double[]>();
            // 计算经度弧度,从弧度转换为角度

            double dLongitude = 2*(Math.Asin(Math.Sin(distance
                                                      /(2*EarthRadius))
                                             /Math.Cos(ToRadians(latitude))));
            dLongitude = ToDegrees(dLongitude);
            // 计算纬度角度
            double dLatitude = distance/EarthRadius;
            dLatitude = ToDegrees(dLatitude);
            // 正方形
            double[] leftTopPoint = {latitude + dLatitude, longitude - dLongitude};
            double[] rightTopPoint = {latitude + dLatitude, longitude + dLongitude};
            double[] leftBottomPoint =
            {
                latitude - dLatitude,
                longitude - dLongitude
            };
            double[] rightBottomPoint =
            {
                latitude - dLatitude,
                longitude + dLongitude
            };
            squareMap.Add("leftTopPoint", leftTopPoint);
            squareMap.Add("rightTopPoint", rightTopPoint);
            squareMap.Add("leftBottomPoint", leftBottomPoint);
            squareMap.Add("rightBottomPoint", rightBottomPoint);
            return squareMap;
        }

        /// <summary>
        ///     将用角度表示的角转换为近似相等的用弧度表示的角。
        ///     从角度到弧度的转换通常是不精确的。
        /// </summary>
        /// <param name="angdeg"></param>
        /// <returns></returns>
        private static double ToRadians(double angdeg)
        {
            return angdeg/180.0*Math.PI;
        }

        /// <summary>
        ///     将用弧度表示的角转换为近似相等的用角度表示的角。
        ///     从弧度到角度的转换通常是不精确的；
        ///     用户不 应该期望 cos(toRadians(90.0)) 与 0.0 完全相等。
        /// </summary>
        /// <param name="angrad"></param>
        /// <returns></returns>
        private static double ToDegrees(double angrad)
        {
            return angrad*180.0/Math.PI;
        }
    }
}