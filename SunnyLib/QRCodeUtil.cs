﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Drawing.Imaging;
using ThoughtWorks.QRCode.Codec;

namespace Sunny.Common
{
    public class QRCodeUtil
    {
        /// <summary>  
        /// 加边框，保存图片  
        /// </summary>  
        /// <param name="strPath">保存路径</param>  
        /// <param name="img">图片</param>  
        public static void SaveImg(string strPath,  Bitmap img)
        {
            //保存图片到目录  
            //if (!Directory.Exists(strPath))
            //{
            //    //当前目录不存在，则创建  
            //    Directory.CreateDirectory(strPath);
            //}
            //文件名称  
            //fileName = fileName == null ? (Guid.NewGuid().ToString().Replace("-", "") + ".png") : fileName;

            Bitmap bitmap = Imager.AddBorder(img, 60, Color.White);
            bitmap.Save(strPath, System.Drawing.Imaging.ImageFormat.Png);
            
        }

        public static void Create_Save_ImgCode(string savePath,string codeNumber, int size)
        {
            Bitmap bitmap = Create_ImgCode(codeNumber, size);
            SaveImg(savePath, bitmap);
        }

        /// <summary>  
        /// 生成二维码图片  
        /// </summary>  
        /// <param name="codeNumber">要生成二维码的字符串</param>       
        /// <param name="size">大小尺寸</param>  
        /// <returns>二维码图片</returns>  
        public static Bitmap Create_ImgCode(string codeNumber, int size)
        {
            //创建二维码生成类  
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            //设置编码模式  
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            //设置编码测量度  
            qrCodeEncoder.QRCodeScale = size;
            //设置编码版本  
            qrCodeEncoder.QRCodeVersion = 0;
            //设置编码错误纠正  
            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            //生成二维码图片  
            System.Drawing.Bitmap image = qrCodeEncoder.Encode(codeNumber);
            return image;
        }  
    }
}